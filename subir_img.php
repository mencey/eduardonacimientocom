<?php 
	
	include("cabecera.php");	
	include("menu.php");
	include("info.php");

	echo '<div id="centro">';
if (($_SESSION['rol'] == 'admin') || (isset($_SESSION['user']))) {
if (!$_POST['action'] == 'submitted') {   
	echo '<div class="asunto">Subir una foto a la galería</div>
	<div class="noticia"><p>Aquí podrás subir nuevas fotos a la galería.</p>
	<p>Para ello pulsa continuar.</p></div>';
	echo '<form action="subir_img.php" method="post">
	<input type="hidden" name="action" value="submitted"></input>';	
	echo '<p><input type="submit" name="submit" value="continuar"></input></p>
	</form>';
}
else {
	echo '<div class="asunto">Subir una foto a la galería</div>
	<div class="noticia">
	<form action="subir_img.php" method="post" enctype="multipart/form-data" name="form1">
	<p align="center">Imagen:
	<input name="archivo" type="file" id="archivo">
	</p>
	<script language="javascript" type="text/javascript" src="tinymce/tiny_mce.js">
  	</script>
   	<script language="javascript" type="text/javascript">
      	tinyMCE.init({
		mode : "textareas"
		});
 	</script>
	<p>Descripción:</p><div class="noticia">
	<textarea name="descripcion" id="descripcion" type="text" class="noticia" 
	cols="40" rows="10"></textarea>
   	</div>	
   	<p>Contenido extra:</p><div class="noticia">
	<textarea name="extra" id="extra" type="text" class="noticia" 
	cols="40" rows="20"></textarea>
   	</div>	
   	<div class="bitacora"><p>Categoría:</p><div class="asunto">
	<input name="categoria" type="text" class="asunto" value=""></input></div>
	<input type="hidden" name="action" value="submitted"></input>
	<input type="hidden" name="password" value="'.$_POST['password'].'"></input>
	<p align="center"><input name="boton" type="submit" id="boton" 
	value="Subir la imágen y guardar los datos"></p>
	</form>';
	if ($_SESSION['rol'] == 'admin') {
		$dir = getcwd();
		$dir .= '/galeria/';	
		$dirmini = $dir . 'mini/mini';
	}
	else {
		$dir = getcwd();
		$dir .= '/album/'.$_SESSION['user'].'/';	
		$dirmini = $dir . 'mini/mini';	
	}
	if($boton) {
		$nombre_completo = $HTTP_POST_FILES['archivo']['name'];
		$dir .= $HTTP_POST_FILES['archivo']['name'];
		$dirmini .= $HTTP_POST_FILES['archivo']['name'];
		if (file_exists($dir)) {
		 	echo '<h1>Ya existe un archivo con ese nombre</h1>';
  	 	}
	 	else {
  	 		if (is_uploaded_file($HTTP_POST_FILES['archivo']['tmp_name'])) {	   
 		   		copy($HTTP_POST_FILES['archivo']['tmp_name'], $dir);
 	      			$subio = true;
	   		}
			if($subio) {
	 	 		echo 'Subida con exito la imagen ' . $nombre_completo;
			}
			else {
 		   		echo "Algo falló";
			}
			$sentencia = 'convert ' . $dir . ' -thumbnail 120x120 ' . $dirmini;         
			exec ($sentencia);
		
			$descripcion = $_POST['descripcion'];
			$categoria = $_POST['categoria'];
			$extra = $_POST['extra'];
			include("config.php");
			if ($_SESSION['rol'] == 'admin') {
				$pre_y_nom = $prefijo_bd . 'galeria';
			}
			else {
				$pre_y_nom = $prefijo_bd . 'album';
			}
			$identificador = "SELECT MAX( identificador ) FROM $pre_y_nom WHERE 1";
			$identificador++;
			if ($_SESSION['rol'] == 'admin') {
			$insertar = "INSERT INTO $pre_y_nom (`identificador`, `url`, `descripcion`, `categoria`, `extra`)
			 VALUES ('$identificador', '$nombre_completo', '$descripcion', '$categoria', '$extra');";
			}
			else {
			$autor = $_SESSION['user'];
			$insertar = "INSERT INTO $pre_y_nom (`identificador`, `url`, `descripcion`, `categoria`, `extra`, `autor`)
			 VALUES ('$identificador', '$nombre_completo', '$descripcion', '$categoria', '$extra', '$autor');";
			}
			@mysql_query($insertar, $conex);
			@mysql_close($conex);
		}
	}
}
}
else {
	echo '<div class="asunto">No tienes permisos:</div>
	<div class="noticia"><p>Lo sentimos, no tienes permiso para subir nuevas imágenes, sólo los administradores pueden hacerlo.</p>
	<p>Si erees un adminisitrador, por favor <a href="registrar.php">inicia sesión</a></p></div>';
}

	
	include("pie.php");
?> 
