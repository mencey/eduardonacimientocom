<?php 
	
	include("cabecera.php");
	include("menu.php");
	include("publicidad.php");
	$contenido_pagina = ob_get_contents ();
   ob_end_clean ();
   echo str_replace ('<title>A la luz de la Libertad', '<title>Calculadora Decathlon y Heptathlon', $contenido_pagina);

	echo '<div id="centro">';
	if (!$_POST['action'] == 'submitted') {   
	echo '<h2>Calculadora de puntos <a href="http://es.wikipedia.org/w/index.php?title=Heptatlón">Heptathlón</a>:</h2>';
	echo '<img src="galeria/400px-Olympic_rings.svg.png" style="float:right; width:400px"/>';
	echo '<form action="calculadora.php" method="post">
	<dl>
	<dt class="lista_datos"><abbr title="En segundos">60ml:</abbr></dt> <dd><input type="text" name="60ml" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">longitud:</abbr></dt> <dd><input type="text" name="longitud" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">peso:</abbr></dt> <dd><input type="text" name="peso" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">altura:</abbr></dt> <dd><input type="text" name="altura" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En segundos">60mv:</abbr></dt> <dd><input type="text" name="60mv" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">Pértiga:</abbr></dt> <dd><input type="text" name="pertiga" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En minutos y en segundos">1000ml:</abbr></dt> <dd><input type="text" name="1000mlmin"  size="5" value="0"></input>
	<input type="text" name="1000mlseg"  size="5" value="0"></input></dd>
	<input type="hidden" name="action" value="submitted"></input>
	<input type="hidden" name="modo" value="hepta"></input>
	</dd>
	<dl>
	<p><input type="submit" name="submit"></button>
	<input type="reset" name="reset"></button></p>
</form>';
	echo '<p class="comentario"><a href="heptathlon.php">Generar informe completo en pdf</a></p>';
	echo '<h2>Calculadora de puntos <a href="http://es.wikipedia.org/w/index.php?title=Decatlón">Decathlón</a>:</h2>';	
	echo '<img src="galeria/Mechanical-Calculator.png" style="float:right; width:400px"/>';
	echo '<form action="calculadora.php" method="post">
	<dl>
	<dt class="lista_datos"><abbr title="En segundos">100ml:</abbr></dt> <dd><input type="text" name="100ml" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">longitud:</abbr></dt> <dd><input type="text" name="longitud" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">peso:</abbr></dt> <dd><input type="text" name="peso" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">altura:</abbr></dt> <dd><input type="text" name="altura" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En segundos">400ml:</abbr></dt> <dd><input type="text" name="400ml" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En segundos">110mv:</abbr></dt> <dd><input type="text" name="110mv" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">disco:</abbr></dt> <dd><input type="text" name="disco" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">Pértiga:</abbr></dt> <dd><input type="text" name="pertiga" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En metros">jabalina:</abbr></dt> <dd><input type="text" name="jabalina" size="5" value="0"></input></dd>
	<dt class="lista_datos"><abbr title="En minutos y segundos">1500ml:</abbr></dt> <dd><input type="text" name="1500mlmin"  size="5" value="0"></input>
	<input type="text" name="1500mlseg"  size="5" value="0"></input></dd>
	<input type="hidden" name="action" value="submitted"></input>
	<input type="hidden" name="modo" value="deca"></input>
	</dd>
	<dl>
	<p><input type="submit" name="submit"></button>
	<input type="reset" name="reset"></button></p>
</form>';
	echo '<p class="comentario"><a href="decathlon.php">Generar informe completo en pdf</a></p>';
	}
	else if ($_POST['modo'] == 'hepta') {
		$resultado = array("60ml" => 0, "longitud" => 0, "peso" => 0, "altura" => 0, "60mv" => 0, "pertiga" => 0, "1000ml" => 0);
		// 60 metros lisos
		$resultado['60ml'] = $_POST['60ml'];
		if ($resultado['60ml'] < 1) {
			$resultado['60ml'] = 11.4;
		}
		$resultado['60ml'] = floor(58.0150 * pow((11.50 - $resultado['60ml']),1.81));
		echo '<div class="bitacora" style="width: 25%;"><div class="asunto">60 metros lisos: </div><div class="noticia">';
		echo $resultado['60ml'];
		echo ' puntos</div>';
		// Longitud
		$resultado['longitud'] = $_POST['longitud'];
		if ($resultado['longitud'] < 2.2){
			$resultado['longitud'] = 2.2; 
		}
		$resultado['longitud'] = floor(0.14354 * pow((($resultado['longitud'] * 100) - 220),1.40));
  		echo '<div class="asunto">Longitud: </div><div class="noticia">';
		echo $resultado['longitud'];
		echo ' puntos</div>';
		// Peso
		$resultado['peso'] = $_POST['peso'];
  		if ($resultado['peso'] < 1.5) {
			$resultado['peso'] = 1.5;
		}
		$resultado['peso'] = floor(51.39 * pow((($resultado['peso']) - 1.5),1.05));
		echo '<div class="asunto">Peso: </div><div class="noticia">';
		echo $resultado['peso'];
		echo ' puntos</div>';
		// Altura
		$resultado['altura'] = $_POST['altura'];
   		if ($resultado['altura'] < 0.75) {
			$resultado['altura'] = 0.75;
		}
   		$resultado['altura'] = floor(0.8465* pow((($resultado['altura'] * 100) - 75.00),1.42));
		echo '<div class="asunto">Altura: </div><div class="noticia">';
		echo $resultado['altura'];
		echo ' puntos</div>';
		// 60 metros vallas
		$resultado['60mv'] =  $_POST['60mv'];
   		if ($resultado['60mv'] > 15.5) {
			$resultado['60mv'] = 15.5;
		}
   		if ($resultado['60mv'] <  1) {
			$resultado['60mv'] = 15.5;
		}
   		$resultado['60mv'] = floor(20.5173 * pow((15.50 - $resultado['60mv']),1.92));
		echo '<div class="asunto">60 metros vallas: </div><div class="noticia">';
		echo $resultado['60mv'];
		echo ' puntos</div>';
		// Pertiga
		$resultado['pertiga'] =  $_POST['pertiga'];
   		if ($resultado['pertiga'] < 1.02) {
			$resultado['pertiga'] = 1.02;
		}
   		$resultado['pertiga'] = floor(0.2797 * pow((($resultado['pertiga'] * 100) - 100.00),1.35));
		echo '<div class="asunto">Pértiga: </div><div class="noticia">';
		echo $resultado['pertiga'];
		echo ' puntos</div>';
		// 1000 ml
		$min = $_POST['1000mlmin'] * 1000;
		$seg = $_POST['1000mlseg'] * 1000;
  		if ($min < 1) { 
			$min = 5000;
   			$seg = 1760.00;	
		}
   		$min = ($min * 60);
   		$min = ($min + $seg);
   		$min = ($min / 1000);
   		$resultado['1000ml'] = floor(0.08713 * pow((305.50 - $min),1.85));
		echo '<div class="asunto">1000 metros: </div><div class="noticia">';
		echo $resultado['1000ml'];
		echo ' puntos</div></div>';
		$tmp = 0;
		foreach ($resultado as $valor) {
			$tmp += $valor;
		}
		$resultado['total'] = $tmp;
		echo '<hr/><div class="bitacora" style="width: 25%; float:right; clear:all"><div class="asunto">Resultado: </div><div class="noticia">';
		echo $resultado['total'];
		echo ' puntos</div></div>';
		echo '<div style="clear:both;"></div><p class="comentario"><a href="calculadora.php">Volver a calcular</a></p>';

		

	}
	else if ($_POST['modo'] == 'deca') {
		
		$resultado = array("100ml" => 0, "longitud" => 0, "peso" => 0, "altura" => 0, "400ml" => 0, "110mv" => 0, "disco" => 0, "pertiga" => 0, "jabalina" => 0, "1500ml" => 0);
		// 100 ml
		$resultado['100ml'] = $_POST['100ml'];
		if ($resultado['100ml'] != 0) {
			$resultado['100ml'] = floor(25.437 * pow((18.0 - $resultado['100ml']),1.81));
		}
		echo '<div class="bitacora" style="width: 25%;"><div class="asunto">100 metros lisos: </div><div class="noticia">';
		echo $resultado['100ml'];
		echo ' puntos</div>';
		// longitud
		$resultado['longitud'] = $_POST['longitud'] * 100;
		if ($resultado['longitud'] != 0) {
			$resultado['longitud'] = floor(0.14354 * pow(($resultado['longitud'] - 220),1.40));
		}
		echo '<div class="asunto">Longitud: </div><div class="noticia">';
		echo $resultado['longitud'];
		echo ' puntos</div>';
		// peso
		$resultado['peso'] = $_POST['peso'];
		if ($resultado['peso'] != 0) {
			$resultado['peso'] = floor(51.39 * pow(($resultado['peso'] - 1.5),1.05));
		}
		echo '<div class="asunto">Peso: </div><div class="noticia">';
		echo $resultado['peso'];
		echo ' puntos</div>';
		// altura
		$resultado['altura'] = $_POST['altura'] * 100;
		if ($resultado['altura'] != 0) {
			$resultado['altura'] = floor(0.8465 * pow(($resultado['altura'] - 75),1.42));
		}
		echo '<div class="asunto">Altura: </div><div class="noticia">';
		echo $resultado['altura'];
		echo ' puntos</div>';
		// 400 ml
		$resultado['400ml'] = $_POST['400ml'];
		if ($resultado['400ml'] != 0) {
			$resultado['400ml'] = floor(1.53775 * pow((82 - $resultado['400ml']),1.81));
		}
		echo '<div class="asunto">400 metros lisos: </div><div class="noticia">';
		echo $resultado['400ml'];
		echo ' puntos</div>';
		// 110 mv
		$resultado['110mv'] = $_POST['110mv'];
		if ($resultado['110mv'] != 0) {
			$resultado['110mv'] = floor(5.74352 * pow((28.5 - $resultado['110mv']),1.92));
		}
		echo '<div class="asunto">110 metros vallas: </div><div class="noticia">';
		echo $resultado['110mv'];
		echo ' puntos</div>';
		// disco
		$resultado['disco'] = $_POST['disco'];
		if ($resultado['disco'] != 0) {
			$resultado['disco'] = floor(12.91 * pow(($resultado['disco'] - 4.0),1.1));
		}
		echo '<div class="asunto">Dsico: </div><div class="noticia">';
		echo $resultado['disco'];
		echo ' puntos</div>';
		// pertiga
		$resultado['pertiga'] = $_POST['pertiga'] * 100;
		if ($resultado['pertiga'] != 0) {
			$resultado['pertiga'] = floor(0.2797 * pow(($resultado['pertiga'] - 100),1.35));
		}
		echo '<div class="asunto">Pertiga: </div><div class="noticia">';
		echo $resultado['pertiga'];
		echo ' puntos</div>';
		// jabalina
		$resultado['jabalina'] = $_POST['jabalina'];
		if ($resultado['jabalina'] != 0) {
			$resultado['jabalina'] = floor(10.14 * pow(($resultado['jabalina'] - 7.0),1.08));
		}
		echo '<div class="asunto">Jabalina: </div><div class="noticia">';
		echo $resultado['jabalina'];
		echo ' puntos</div>';
		// 1500 ml
		$resultado['1500ml'] = $_POST['1500mlmin'] * 60;
		$resultado['1500ml'] += $_POST['1500mlseg'];
		if ($resultado['1500ml'] != 0) {
			$resultado['1500ml'] = floor(0.03768 * pow((480 - $resultado['1500ml']),1.85));
		}
		echo '<div class="asunto">1500 metros lisos: </div><div class="noticia">';
		echo $resultado['1500ml'];
		echo ' puntos</div>';
		echo '</div>';
		$tmp = 0;
		foreach ($resultado as $valor) {
			$tmp += $valor;
		}
		$resultado['total'] = $tmp;
		echo '<hr/><div class="bitacora" style="width: 25%; float:right; clear:all"><div class="asunto">Resultado: </div><div class="noticia">';
		echo $resultado['total'];
		echo ' puntos</div></div>';
		echo '<div style="clear:both;"></div><p class="comentario"><a href="calculadora.php">Volver a calcular</a></p>';
	}
	
	echo '<p class="comentario"><a href="http://www.iaaf.org/downloads/scoringTables/index.html">Tablas de puntos IAAF</a></p>';
	include("pie.php");
?>
