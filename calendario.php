<?php

$array_dia['Monday'] = 'Lunes';
$array_dia['Tuesday'] = 'Martes';
$array_dia['Wednesday'] = 'Miércoles';
$array_dia['Thursday'] = 'Jueves';
$array_dia['Friday'] = 'Viernes';
$array_dia['Saturday'] = 'Sábado';
$array_dia['Sunday'] = 'Domingo';

$array_mes['January'] = 'Enero';
$array_mes['February'] = 'Febrero';
$array_mes['March'] = 'Marzo';
$array_mes['April'] = 'Abril';
$array_mes['May'] = 'Mayo';
$array_mes['June'] = 'Junio';
$array_mes['July'] = 'Julio';
$array_mes['August'] = 'Agosto';
$array_mes['September'] = 'Septiembre';
$array_mes['October'] = 'Octubre';
$array_mes['November'] = 'Noviembre';
$array_mes['December'] = 'Diciembre';


$fecha = explode('|', $fecha_tmp);
$nombredia = $fecha[0];
$dia = $fecha[1];
$nombremes = $fecha[2];
$año = $fecha[3];

if (isset($cal_comentario)) {
echo '<div class="calendario">';
echo '<span style=" display: block;">' . $dia . '/';
echo $array_mes[$nombremes] . '</span>';
echo '<span style="background: #aaa; color: #000; display: block;">' . $año . '</span>
</div>';
}
else if (isset($historial)) {
echo '<div class="calendario">';
echo '<span style=" display: block;">' . $dia . '</span>
</div>';
}
else {
echo '<div class="calendario">' . $array_dia[$nombredia];
echo '<span style="font-size: x-large; width: 100%; display: block;
 padding:6px 0px">' . $dia . '</span>';
echo '<span style="display: block;">' . $array_mes[$nombremes] . '</span>';
echo '<span style="background: #aaa; color: #000; display: block;">' . $año . '</span>
</div>';
}
?>
