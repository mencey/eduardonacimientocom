<?php

include("config.php");
$pre_y_nom = $prefijo_bd . 'acerca';
$tabla = 'CREATE TABLE IF NOT EXISTS ' . $pre_y_nom . ' (
  `identificador` int(11) NOT NULL auto_increment,
  `asunto` text,
  `noticia` text,
  PRIMARY KEY  (`identificador`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';
@mysql_query($tabla, $conex);

$pre_y_nom = $prefijo_bd . 'base';
$tabla = 'CREATE TABLE IF NOT EXISTS ' . $pre_y_nom . ' (
  `identificador` int(11) NOT NULL auto_increment,
  `asunto` text collate latin1_spanish_ci,
  `noticia` text collate latin1_spanish_ci,
  `fecha` text collate latin1_spanish_ci,
  `categoria` text collate latin1_spanish_ci,
  PRIMARY KEY  (`identificador`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;';
@mysql_query($tabla, $conex);

$pre_y_nom = $prefijo_bd . 'coment';
$tabla = 'CREATE TABLE IF NOT EXISTS ' . $pre_y_nom . ' (
  `autoident` int(11) NOT NULL auto_increment,
  `identificador` int(11) NOT NULL default \'0\',
  `asunto` text character set utf8 collate utf8_spanish_ci NOT NULL,
  `noticia` text character set utf8 collate utf8_spanish_ci NOT NULL,
  `ip` text collate latin1_spanish_ci NOT NULL,
  PRIMARY KEY  (`autoident`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;';
@mysql_query($tabla, $conex);

$pre_y_nom = $prefijo_bd . 'contacto';
$tabla = 'CREATE TABLE IF NOT EXISTS ' . $pre_y_nom . ' (
  `identificador` int(11) NOT NULL auto_increment,
  `asunto` text collate latin1_spanish_ci,
  `noticia` text collate latin1_spanish_ci,
  `fecha` text collate latin1_spanish_ci,
  `nombre` text collate latin1_spanish_ci,
  `correo` text collate latin1_spanish_ci,
  PRIMARY KEY  (`identificador`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1';
@mysql_query($tabla, $conex);

$pre_y_nom = $prefijo_bd . 'enlaces';
$tabla = 'CREATE TABLE IF NOT EXISTS ' . $pre_y_nom . ' (
  `identificador` int(11) NOT NULL auto_increment,
  `asunto` text,
  `noticia` text,
  PRIMARY KEY  (`identificador`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';
@mysql_query($tabla, $conex);

$pre_y_nom = $prefijo_bd . 'galeria';
$tabla = 'CREATE TABLE IF NOT EXISTS ' . $pre_y_nom . ' (
  `identificador` int(11) NOT NULL auto_increment,
  `url` text collate latin1_spanish_ci NOT NULL,
  `descripcion` text collate latin1_spanish_ci,
  `categoria` text collate latin1_spanish_ci,
  `extra` text collate latin1_spanish_ci,
  PRIMARY KEY  (`identificador`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;';
@mysql_query($tabla, $conex);

$pre_y_nom = $prefijo_bd . 'galcoment';
$tabla = 'CREATE TABLE IF NOT EXISTS ' . $pre_y_nom . ' (
  `autoident` int(11) NOT NULL auto_increment,
  `identificador` int(11) NOT NULL default \'0\',
  `asunto` text character set utf8 collate utf8_spanish_ci NOT NULL,
  `noticia` text character set utf8 collate utf8_spanish_ci NOT NULL,
  `ip` text collate latin1_spanish_ci NOT NULL,
  PRIMARY KEY  (`autoident`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;';
@mysql_query($tabla, $conex);

$pre_y_nom = $prefijo_bd . 'acerca';
$tabla = 'INSERT INTO ' . $pre_y_nom . ' (`identificador`, `asunto`, `noticia`) VALUES 
(1, \'Acerca de...\', \'\');';
@mysql_query($tabla, $conex);

$pre_y_nom = $prefijo_bd . 'enlaces';
$tabla = 'INSERT INTO ' . $pre_y_nom . ' (`identificador`, `asunto`, `noticia`) VALUES 
(1, \'Enlaces ...\', \'\');';
@mysql_query($tabla, $conex);

@mysql_close($conex);
?>
