<?php 
$frase = $_GET['frase'];
include ('euclides/cifrar.php');
header('Content-type: image/png');


$alto = 120;
$ancho = 50;
$image = imagecreatetruecolor($alto, $ancho);



$back = imagecolorallocate($image, 220, 226, 220);
$border = imagecolorallocate($image, 0, 0, 0);
imagefilledrectangle($image, 0, 0, $alto - 1, $ancho - 1, $back);
imagerectangle($image, 0, 0, $alto - 1, $ancho - 1, $border);



$tam = strlen($frase);
for ($i = 0; $i < $tam; $i++) {
	
	$negro	= imagecolorallocate($image, rand(0, 180), rand(0, 180), rand(0, 180));
	$op = rand(0,2);
	if ($op == 0) {
		imageellipse($image, rand(0, 120), rand(0, 50), rand(0, 120), rand(0, 50), $negro);
	}
	else if ($op == 1) {
		imageline($image, rand(0, 120), rand(0, 50), rand(0, 120), rand(0, 50), $negro);
	}
	else {
		imagerectangle ($image, rand(0, 120), rand(0, 50), rand(0, 120), rand(0, 50), $negro);
	}
	$caracter = $frase[$i];
	imagestring($image, 10, 2 + (14 * ($i + 1)), rand(2, 24), $caracter, $negro);
}

imagepng($image);
imagedestroy($image);
include ('euclides/descifrar.php');
?> 
