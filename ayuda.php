<?php 

	echo '<div class="bitacora">';
	echo '<div class="asunto">';
	echo 'Menú de ayuda de instalación';
	echo '</div><div class="noticia">';
	echo '<p>Si aún no has configurado la bitácora desde el instalador, accede a él <a href="install.php">desde aquí</a>.</p>';
	echo '<p>Si ya lo instalastes, pero deseas cambiar algún parámetro del estilo, <a href="modificarcss.php">entra aquí</a>.</p>';
	echo '<p>Si crees que ya lo tienes instalado y configurado borra o renombra los archivos "install.php" y "modificarcss.php".</p>';
	echo '</div></div>';
?>
